#include <stdio.h>
#include <time.h>
#include <Windows.h>
struct Date
{	
	unsigned short hrs : 5;
	unsigned short min : 7;
	unsigned short sec : 7;
	unsigned short day : 5;
	unsigned short weekday : 3;
	unsigned short month : 4;
	unsigned short year : 7;
};

enum weekday {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday
};


union SignShort
{
	signed short x;
	struct Bytes {
		unsigned short znach : 15;
		unsigned short znak : 1;
	}bites;
}chs;

union Float {
	float x;
	struct Bites {
		unsigned int b1 : 1;
		unsigned int b2 : 1;
		unsigned int b3 : 1;
		unsigned int b4 : 1;
		unsigned int b5 : 1;
		unsigned int b6 : 1;
		unsigned int b7 : 1;
		unsigned int b8 : 1;
		unsigned int b9 : 1;
		unsigned int b10 : 1;
		unsigned int b11 : 1;
		unsigned int b12 : 1;
		unsigned int b13 : 1;
		unsigned int b14 : 1;
		unsigned int b15 : 1;
		unsigned int b16 : 1;
		unsigned int b17 : 1;
		unsigned int b18 : 1;
		unsigned int b19 : 1;
		unsigned int b20 : 1;
		unsigned int b21 : 1;
		unsigned int b22 : 1;
		unsigned int b23 : 1;
		unsigned int b24 : 1;
		unsigned int b25 : 1;
		unsigned int b26 : 1;
		unsigned int b27 : 1;
		unsigned int b28 : 1;
		unsigned int b29 : 1;
		unsigned int b30 : 1;
		unsigned int b31 : 1;
		unsigned int b32 : 1;
	} bites;

	struct Bytes {
		unsigned int byte1 : 8;
		unsigned int byte2 : 8;
		unsigned int byte3 : 8;
		unsigned int byte4 : 8;
	}bytes;

	struct Msz {
		unsigned int mantis : 23;
		unsigned int step : 8;
		unsigned int znak : 1;

	}msz;
} fl; 

void dayweek(Date now) {

	switch (now.weekday)
	{
	case Monday: printf("Понеділок ");
		break;
	case Tuesday: printf("Вівторок ");
		break;
	case Wednesday: printf("Середа ");
		break;
	case Thursday: printf("Четвер ");
		break;
	case Friday: printf("П'ятниця ");
		break;
	case Saturday: printf("Субота ");
		break;
	case Sunday: printf("Неділя ");
		break;
	}

	printf("%d:%d:%d %d.%d.%d\n", now.hrs, now.min, now.sec, now.day, now.month, now.year + 2000);

}

int main()
{
	Date now = {20, 42, 11, 10,Sunday, 11, 22 };
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	dayweek(now);

	printf("Завдання 1\n");

	printf("Кількість байт, що займає структура: %d\n", sizeof(Date));
	printf("Кількість байт, що займає структура time.h: %d\n", sizeof(tm));
	system("pause");

	printf("Завдання 2\n");

	printf("Введіть число x="); scanf_s("%d", &chs);
	printf("\nПри використанні структури данних маємо таку відповідь: ");
	if (chs.bites.znak == 1 && chs.bites.znach !=0)
		printf("число від'ємне\n");
	else if (chs.bites.znak == 0 && chs.bites.znach != 0)
		printf("число додатнє\n");
	else
		printf("число нейтральне\n");
	printf("\nПри використанні логічний операцій маємо таку відповідь: ");
	if (chs.x < 0)
		printf("число від'ємне\n");
	else if (chs.x > 0)
		printf("число додатнє\n");
	else printf("число нейтральне\n");
	printf("Значення числа: %d\n", chs.x);
	system("pause");


	printf("Завдання 3\n");

		signed char a;
		a = 5 + 127;
		printf("5 + 127 = %d\n", a);
		
		a = 2 - 3;
		printf("2 - 3 = %d\n", a);

		a = -120 - 34;
		printf("-120 - 34 = %d\n", a);

		a = unsigned char(-5);
		printf("unsigned char (-5) = %d\n", a);

		a = 56 & 38;
		printf("56 & 38 = %d\n", a);

		a = 56 | 38;
		printf("56 | 38 = %d\n", a);
		system("pause");
		

		printf("Завдання 4\n");
		
		printf("Введіть дробове число\nx="); scanf_s("%f", &fl.x);
		printf("Вивід побітово: ");
		printf("%d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d", fl.bites.b32, fl.bites.b31,
			fl.bites.b30, fl.bites.b29, fl.bites.b28, fl.bites.b27, fl.bites.b26, fl.bites.b25, fl.bites.b24, fl.bites.b23,
			fl.bites.b22, fl.bites.b21, fl.bites.b20, fl.bites.b19, fl.bites.b18, fl.bites.b17, fl.bites.b16,
			fl.bites.b15, fl.bites.b14, fl.bites.b13, fl.bites.b12, fl.bites.b11, fl.bites.b10, fl.bites.b9,
			fl.bites.b8, fl.bites.b7, fl.bites.b6, fl.bites.b5, fl.bites.b4, fl.bites.b3, fl.bites.b2,
			fl.bites.b1);
		printf("\nВивід побайтово: ");
		printf("%x %x %x %x", fl.bytes.byte4, fl.bytes.byte3, fl.bytes.byte2, fl.bytes.byte1);
		printf("\nЗнак: %d", fl.msz.znak);
		printf("\nПорядок значення: %d", fl.msz.step);
		printf("\nМантиса: %d\n", fl.msz.mantis);
		system("pause");

				
	return 0;
}
